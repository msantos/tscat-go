// Tscat timestamps standard input to standard output, standard error
// or both.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"syscall"
)

type State struct {
	l []*log.Logger
}

const (
	version = "0.1.0"
)

func usage() {
	fmt.Fprintf(os.Stderr, `%s v%s
Usage: %s [<option>] [label]

Options:

`, path.Base(os.Args[0]), version, os.Args[0])
	flag.PrintDefaults()
}

func stdio(output int, label string) []*log.Logger {
	if label != "" {
		label += " "
	}
	l := make([]*log.Logger, 0)
	if output&syscall.Stdout != 0 {
		l = append(l, log.New(os.Stdout, label, log.Lmsgprefix|log.LstdFlags))
	}
	if output&syscall.Stderr != 0 {
		l = append(l, log.New(os.Stderr, label, log.Lmsgprefix|log.LstdFlags))
	}
	return l
}

func main() {
	help := flag.Bool("help", false, "Display usage")
	output := flag.Int("output", 1, "stderr=1, stderr=2, both=3")

	flag.Usage = func() { usage() }
	flag.Parse()

	if *help {
		usage()
		os.Exit(2)
	}

	label := ""
	if flag.NArg() > 0 {
		label = flag.Arg(0)
	}

	s := &State{
		l: stdio(*output, label),
	}

	if err := s.run(); err != nil {
		log.Fatalln(err)
	}
}

func (s *State) run() error {
	stdin := bufio.NewScanner(os.Stdin)

	for stdin.Scan() {
		for _, v := range s.l {
			v.Println(stdin.Text())
		}
	}

	return stdin.Err()
}
